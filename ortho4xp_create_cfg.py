from pathlib import Path


class Region(object):

    name = ""

    tiles_boxes = []

    mesh_zl = 18

    cover_airports_with_highres = True
    cover_extent = 2
    cover_zl = 18
    default_website = ""
    default_zl = 17
    curvature_tol = 1.5

    custom_dem = ""
    custom_dem_is_needed = False

    def is_within_region(self, lat, lon):
        for box in self.tiles_boxes:
            if box[0] <= lat <= box[2] and box[1] <= lon <= box[3]:
                return True
        return False

    def region_tiles(self):
        for box in self.tiles_boxes:
            tiles = [(lat, lon) for lat in range(box[0], box[2] + 1) for lon in range(box[1], box[3] + 1)]
            for tile in tiles:
                yield tile

    def set_custom_dem(self, lat, lon):
        self.custom_dem = ""

    def write_cfg(self, lat, lon):

        cfg_template = """# Cfg generated: Region = {region_name} 
apt_smoothing_pix=8
road_level=1
road_banking_limit=0.5
lane_width=5.0
max_levelled_segs=100000
water_simplification=0.0
min_area=0.001
max_area=200.0
clean_bad_geometries=True
mesh_zl={mesh_zl}
curvature_tol={curvature_tol}
apt_curv_tol=0.5
apt_curv_ext=0.5
coast_curv_tol=1.0
coast_curv_ext=0.5
limit_tris=0
hmin=0.0
min_angle=10.0
sea_smoothing_mode=zero
water_smoothing=10
iterate=0
mask_zl=14
masks_width=100
masking_mode=sand
use_masks_for_inland=False
imprint_masks_to_dds=False
masks_use_DEM_too=False
masks_custom_extent=
cover_airports_with_highres={cover_airports_with_highres}
cover_extent={cover_extent}
cover_zl={cover_zl}
cover_screen_res=HD_1080p
cover_fov=60.0
cover_fpa=10.0
cover_greediness=1
cover_greediness_threshold=0.7
ratio_water=0.25
overlay_lod=25000.0
sea_texture_blur=0.0
add_low_res_sea_ovl=False
experimental_water=0
normal_map_strength=1.0
terrain_casts_shadows=True
use_decal_on_terrain=True
custom_dem={custom_dem}
fill_nodata=True
default_website={default_website}
default_zl={default_zl}
zone_list=[]
    """

        self.set_custom_dem(lat, lon)
        if self.custom_dem_is_needed and not self.custom_dem:
            print("no custom dem for this lat, lon")
            return

        tile_name = f"zOrtho4XP_+{lat:02}+{lon:03}"
        tiles_root = Path("g:/Ortho4XP_v130/Tiles")
        tile_dir = Path(tiles_root).joinpath(tile_name)
        print(f"tile: {tile_dir}")
        if not tile_dir.is_dir():
            tile_dir.mkdir()

        file_cfg = tile_dir.joinpath(f"Ortho4XP_+{lat:02}+{lon:03}.cfg")

        mesh_zl = max(self.cover_zl, self.default_zl, ) if self.cover_airports_with_highres else self.default_zl
        mesh_zl = max(self.mesh_zl, mesh_zl)

        file_content = cfg_template.format(
            region_name=self.name,
            lat=lat, lon=lon,
            cover_airports_with_highres=self.cover_airports_with_highres,
            cover_extent=self.cover_extent,
            cover_zl=self.cover_zl,
            default_website=self.default_website,
            default_zl=self.default_zl,
            curvature_tol=self.curvature_tol,
            mesh_zl=mesh_zl,
            custom_dem=self.custom_dem,
        )

        with open(file_cfg, "w") as f:
            print(f"cfg : {file_cfg}")
            f.write(file_content)

        return


class Italia(Region):

    name = "Italia"

    tiles_boxes = [
        (46,  8, 46, 13),
        (44,  6, 45, 13),
        (43,  7, 45, 13),
        (38, 10, 42, 15),
        (39, 16, 41, 17),
        (39, 18, 40, 18),
        (38,  8, 41,  9),
        (39, 16, 41, 17),
        (36, 11, 39, 17),
        (35, 11, 35, 14),
    ]

    custom_dem_is_needed = False

    default_website = "Here"
    default_zl = 17
    curvature_tol = 1.5
    cover_airports_with_highres = True
    cover_extent = 2
    cover_zl = 18

    def set_custom_dem(self, lat, lon):

        custom_dem_file = Path(f"E:/Ortho4XP_v130/custom_dem/{lat:02}_{lon:03}.dem ")
        if not custom_dem_file.is_file():
            # no custom dem for this lat, lon
            self.custom_dem = ""
        else:
            self.custom_dem = custom_dem_file


class SardegnaCorsica(Italia):

    name = "Sardegna&Corsica"

    tiles_boxes = [
        (38,  8, 42,  9),
    ]

    default_website = "GO2"


if __name__ == '__main__':

    processed_tiles = []

    regions = (SardegnaCorsica, Italia)

    for region_class in regions:
        region = region_class()
        for tile in region.region_tiles():
            if tile not in processed_tiles:
                print(f"*** Region {region.name}: {tile[0]:02} {tile[1]:03}")
                region.write_cfg(tile[0], tile[1])
                processed_tiles.append(tile)
